#README

!IMPORTANT NOTE!:

The test "test_should_create_checkin", in the file test/controllers/checkins_controller_test.rb
is failing. This should test the create method for checkins. I have attempted
numerous solutions to this, both by modifying the test and by modifying the 
function but I cannot get it to pass. However, the application behaviour
still seems to be normal and as wanted. Please still run the application 
using '$ rails server' despite this test not working, so that you may see how 
the application works!


Description of application:

The application contains two data models, both created using rails g scaffold. 
The event model has an event_name attribute, which is a string, and an 
event_datetime attribute, which is of datetime datatype. The checkin model
has two attributes, event_id (created using event:references), and email, which
is a string. Thus, an event has many checkins and a checkin belongs to an event.
These models were created using scaffold, but the pages and linking between the
pages were modified. Modifications include: the root page has been set to the 
event index page, the 'show' page for events has been modified to display a 
table of all attendees (checkins) with emails and checkin times (as recorded by 
Active Record in the created_at column), the checkin index page is not available
to the user, etc. The notice messages, link names, button labels, and form 
layouts have been modified to make them user friendly. E.g. a drop down menu 
with event names was created on the new checkin form, the form for editing 
checkins was modified so it is only about editing the email address (without 
changing the associated event or event_id), etc.

All model validations have been tested, and all public methods have been tested.
All features that must be implemented have been tested. "Create event" and
"Cancel event" are tested via test/controllers/events_controller_test.rb. "Can't check
in more than once" is tested via a checkin model validation test in test/models/checkin_test.rb.
"Record date and time of check-in" is tested via test/controllers/checkins_controller_test.rb. 
"View a list of attendees with their check-in times for each event" is tested 
via test/controllers/events_controller_test.rb. The only feature that could not 
be successfully tested was "Check into an event using email address", as the 
test written for the create method for checkins was not passing (as noted above).



To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --without production
```

Next, migrate the database:

```
$ rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ rails server
```
