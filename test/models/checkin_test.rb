require 'test_helper'

class CheckinTest < ActiveSupport::TestCase
  
       
	test "the truth" do
     assert true
   end

def setup
	@event = events(:one)
	@checkin1 = checkins(:one)
	@checkin = Checkin.new(email: "malak.khan@yale.edu", event_id: @event.id)
end


test 'valid checkin' do
	assert @checkin.valid?
end

test 'checkin datetime recorded by created_at' do
	assert @checkin1.created_at.present?
end	

test 'cannot check in twice to an event with same email' do
	checkin2 = Checkin.new(event_id: @checkin.event_id, email: @checkin.email)
	@checkin.save(validate: false)
	refute checkin2.valid?
end

test 'invalid with incorrect format email' do
	@checkin.email = "malak@yale"
        refute @checkin.valid?
end

test 'invalid with incorrect format email 2' do
	@checkin.email = "malak"
        refute @checkin.valid?
end


test 'invalid without email' do
	@checkin.email = ""
	refute @checkin.valid?, 'checkin is valid without an email'
	assert_not_nil @checkin.errors[:email], 'no validation error for email present'
end

test 'invalid without event_id' do
	@checkin.event_id = ""
	refute @checkin.valid?, 'checkin is valid without event_id'
	assert_not_nil @checkin.errors[:event_id], 'no validation error for event_id present'
end

end
