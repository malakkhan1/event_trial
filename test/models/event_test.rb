require 'test_helper'

class EventTest < ActiveSupport::TestCase
 
	test "the truth" do
    assert true
  end

def setup
	@event = Event.new(event_name: "Party", event_datetime: "2019-05-06 18:48:00")
end

test 'valid event' do
	assert @event.valid?
end	

test 'invalid without name' do
	@event.event_name = ""
        refute @event.valid?, 'event is valid without a name'
        assert_not_nil @event.errors[:email], 'no validation error for name present'
end

test 'invalid without datetime' do
	@event.event_datetime = ""
        refute @event.valid?, 'event is valid without datetime'
        assert_not_nil @event.errors[:email], 'no validation error for datetime present'
end

end
