class Checkin < ApplicationRecord
  belongs_to :event

  validates :email, presence: true
  validates :event_id, presence: true

  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create

  validates :email, uniqueness: { scope: :event_id }

end
