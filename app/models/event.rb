class Event < ApplicationRecord
has_many :checkins, dependent: :destroy
validates :event_name, presence: true
validates :event_datetime, presence: true

end
