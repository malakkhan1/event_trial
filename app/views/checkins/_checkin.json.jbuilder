json.extract! checkin, :id, :email, :event_id, :created_at, :updated_at
json.url checkin_url(checkin, format: :json)
